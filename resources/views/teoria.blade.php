    @extends('templates.base')

    @section('conteudo')
    <main>
    <h2>Pilhas</h2>
    <hr>
    <p>
      As pilhas são dispositivos que armazenam energia usando propriedades químicas de certos compostos, 
      para transformar energia química em energia elétrica.
      São formadas por dois eletrodos (positivo e negativo) onde ocorrem respectivamente as 
      semirreações de redução e oxidação, além de um eletrólito, que é uma solução condutora de íons.
    </p>

    <ul>
      
      <li> Pilhas alcalinas: utilizam zinco como ânodo e dióxido de manganês como cátodo,
         com um eletrólito alcalino (geralmente hidróxido de potássio).
      </li>
      <li>
        Pilhas de zinco-carbono: similares às alcalinas, mas com um eletrólito mais ácido 
        (geralmente cloreto de amônio).
      </li>
      <li>
        Baterias de íon-lítio: amplamente usadas em dispositivos eletrônicos portáteis, 
        como celulares e laptops. O cátodo é composto por óxido de lítio e cobalto, 
        o ânodo por grafite e o eletrólito por sais de lítio dissolvidos em solventes orgânicos.
      </li>
      <li>
       Baterias de chumbo-ácido: usadas em veículos automotores e sistemas de energia em alguns casos. Possuem placas de chumbo no ânodo e no cátodo, e o eletrólito é uma solução de ácido sulfúrico.
      </li>
      <li>
       Baterias de níquel-cádmio: embora menos comuns atualmente, utilizavam cádmio no ânodo, óxido de níquel no cátodo e um eletrólito alcalino.
      </li>
    </ul>

    <h2>Teoria</h2>
    <hr>
    <p>
  Uma fonte de tensão real é constituída de uma fonte de tensão ideal (E) em série com a sua resistência interna para sabermos é necessário conhecer a tensão interna fonte sem carga ligada e quando conectada com a carga  mostrando que a corrente que circulará vai produzir a queda de tensão interna (Vr) e a queda de tensão externa (VR).
  Usando a a equação do divisor se tensão usamos a equação r=R(E - I)/Vr,podemos medir indiretamente  a resistência de uma pilha ou baterias.
    </p>
    <img src="/imgs/CodeCogsEqn (3).png" width="200px">

</body>
</html>
  </main>
  @endsection
  @section ('rodape')
  @endsection